from app.models import MyUser
from app.serializers import UserSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status

import datetime


@api_view(['POST'])
def user_creation(request):
    """
    This function creates the user.
    """
    if request.method == 'POST':
        serializer = UserSerializer(data=request.data)

        if serializer.is_valid():
            user = serializer.save()
            return Response(
                {"user_id": user.id},
                status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def user_login(request):
    """
    This function allow user to login after authenticating given email and
    password.
    """
    if request.method == 'POST':
        email = request.data['email']
        password = request.data['password']
        user = check_user(email, password)

        if user:
            return Response(
                {"message": "Logged in successfully"},
                status=status.HTTP_200_OK)
        else:
            return Response(
                {"message": "Invalid credentials"},
                status=status.HTTP_401_UNAUTHORIZED)


def check_user(email, password):
    """
    Check that user exists or not. If yes then compare the password.
    """
    try:
        user = MyUser.objects.get(email=email)
        if user.password == password:
            return user
        else:
            return None
    except MyUser.DoesNotExist:
        return None


@api_view(['GET'])
def user_detail(request, pk):
    """
    This function is used to get the details of user specified in the url.
    """
    if request.method == 'GET':
        try:
            user = MyUser.objects.get(id=pk)
            return Response(
                {"email": user.email,
                 "name": user.name,
                 "dob": user.date_of_birth},
                status=status.HTTP_200_OK)
        except MyUser.DoesNotExist:
            return Response(
                {"message": "Invalid user_id"},
                status=status.HTTP_401_UNAUTHORIZED)


@api_view(['GET', 'POST'])
def change_password(request, pk):
    """
    This function is used to change user password and also store Attempts
    of each password changed. Max 3 attempts can be made.
    """
    if request.method == 'POST':
        new_password = request.data['password']
        try:
            user = MyUser.objects.get(id=pk)
            user.password = new_password
            store_attempts(user)
            user.save()
            return Response(
                {"message": "Password changed successfully"},
                status=status.HTTP_200_OK)
        except MyUser.DoesNotExist:
            return Response(
                {"message": "Invalid user_id"},
                status=status.HTTP_401_UNAUTHORIZED)
    if request.method == 'GET':
        try:
            user = MyUser.objects.get(id=pk)
            attempt_list = get_attempt_list(user)
            return Response(
                attempt_list,
                status=status.HTTP_200_OK)
        except MyUser.DoesNotExist:
            return Response(
                {"message": "Invalid user_id"},
                status=status.HTTP_401_UNAUTHORIZED)


def store_attempts(user):
    """
    It stores the date when attempts have been made to change password.
    """
    if user.attempt_1 is None:
        user.attempt_1 = datetime.date.today()
    elif user.attempt_2 is None:
        user.attempt_2 = datetime.date.today()
    elif user.attempt_3 is None:
        user.attempt_3 = datetime.date.today()
    else:
        return Response(
            {"message": "Attempts over!"},
            status=status.HTTP_403_FORBIDDEN)
    user.save()


def get_attempt_list(user):
    """
    Helps to get the list of attempts user have made to change_password.
    """
    attempt_list = []
    if user.attempt_1:
        attempt_list.append(user.attempt_1)
        if user.attempt_2:
            attempt_list.append(user.attempt_2)
            if user.attempt_3:
                attempt_list.append(user.attempt_3)
    return attempt_list
