from django.conf.urls import url, patterns

urlpatterns = patterns('app.views',
    url(r'^register/$', 'user_creation', name='user_creation'),
    url(r'^login/$', 'user_login', name='user_login'),
    url(r'^user/(?P<pk>[0-9]+)/$', 'user_detail', name='user_detail'),
    url(r'^user/(?P<pk>[0-9]+)/change_password/$',
        'change_password',
        name='change_password'),
)
