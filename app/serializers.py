from rest_framework import serializers
from app.models import MyUser


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = MyUser
        fields = ('id', 'email', 'password', 'date_of_birth', 'name')


class UserLoginSerializer(serializers.ModelSerializer):

    class Meta:
        model = MyUser
        fields = ('id', 'email', 'password')
