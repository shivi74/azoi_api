# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='myuser',
            old_name='attemp_1',
            new_name='attempt_1',
        ),
        migrations.RenameField(
            model_name='myuser',
            old_name='attemp_2',
            new_name='attempt_2',
        ),
        migrations.RenameField(
            model_name='myuser',
            old_name='attemp_3',
            new_name='attempt_3',
        ),
    ]
